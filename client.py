import requests
import json
import pandas as pd

headers = {'Content-type': 'application/json'}

url = 'http://127.0.0.1:5000/ingest'
samples = 7

# filename = './Left_2019-07-15T17.44.26.944_C69CCC01B04E_EulerAngles.csv'
filename = './euler-angles-small-set.csv'

data = pd.read_csv(filename)

rows = data.shape[0]

i = 0
while i < rows:
    n = i + samples
    result = data[i:n].to_json(orient="records")
    parsed = json.loads(result)
    params = json.dumps(parsed, indent=4)
    print('>>>>> OUT: ', params)
    r = requests.post(url=url, json=params, headers=headers)
    content = r.content
    print('>>>>> IN: ', content.decode('UTF-8'))
    i += samples

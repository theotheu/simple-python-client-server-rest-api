import flask
from flask import request
import json

app = flask.Flask(__name__)
app.config["DEBUG"] = True


@app.route('/sayhi', methods=['GET'])
def home():
    return "Hey there!"


@app.route('/ingest', methods=['GET', 'POST'])
def ingesthandler():
    print('IN', request.is_json)
    content = request.get_json()
    print(content)

    data = {}
    data['result'] = 'OK'
    params = json.dumps(data)

    print('OUT', params)
    return params


app.run()

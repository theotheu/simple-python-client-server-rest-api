

# Installation & configuration (Linux)
1. `git clone https://gitlab.com/theotheu/simple-python-client-server-rest-api/`
1. `cd simple-python-client-server-rest-api/` 
1. `python3.6 -m venv venv`
1. `source venv/bin/activate`
1. `pip install --upgrade pip`
1. `pip install requirements.txt`

# Installation & Configuration (MacOSX)
Replace step #3 from Linux with

- `pip install virtualenv`
- `virtualenv venv`

# Startup server
1. `python server.py`

# Run client
1. `python client.py`

## Options
1. Change filename. There are two files. A big (11MB) file for a complete session and a shorter one with only 100 samples
1. Change the endpoint url (*not available yet*)
1. Change the number of samples 